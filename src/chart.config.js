export const chartConfig = {
  elements: {
    point: {
      radius: 0,
      hoverRadius: 2,
    },
    line: {
      borderWidth: 2,
      tension: 0,
    },
  },
  hover: {
    mode: 'index',
    intersect: false,
    position: 'nearest',
  },
  legend: {
    align: 'center',
    display: true,
    position: 'bottom',
    labels: {
      usePointStyle: true,
    },
  },
  maintainAspectRatio: false,
  responsive: true,
  scales: {
    yAxes: [
      {
        scaleLabel: {
          display: false,
        },
        ticks: {
          stepSize: 10,
        },
      },
    ],
    xAxes: [
      {
        display: false,
      },
    ],
  },
  tooltips: {
    mode: 'index',
    intersect: false,
    position: 'nearest',
  },
};
