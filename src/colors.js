const _colors = [
  '#4CAF50',
  '#FF9800',
  '#673AB7',
  '#607D8B',
  '#00BCD4',
  '#8BC34A',
  '#FFC107',
  '#CDDC39',
];

export const Colors = {
  getColor: (index) => _colors[index % _colors.length],
};
