export async function getChartData() {
  const timeout = new Promise((resolve) => setTimeout(resolve, 1500));
  const get = fetch('http://192.168.178.35:3000/kraken', {
    headers: {
      Accept: 'application/json',
      'Content-type': 'application/json',
    },
  });

  const [_, res] = await Promise.all([timeout, get]);

  if (res.ok) {
    return await res.json();
  } else {
    throw new Error();
  }
}
